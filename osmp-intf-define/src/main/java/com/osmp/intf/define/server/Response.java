 /*   
 * Project: OSMP
 * FileName: Response.java
 * version: V1.0
 */
package com.osmp.intf.define.server;

import java.io.Serializable;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2017年2月7日 上午11:57:20上午10:51:30
 */
public class Response implements Serializable{
	
	private static final long serialVersionUID = -7706477558927160460L;
	
	private final String msgId;
	
	private final String code;
	
	private final String descript;
	
	private final Object result;
	

	/**
	 * @param msgId
	 * @param code
	 * @param descript
	 * @param result
	 */
	public Response(String msgId, String code, String descript, Object result) {
		super();
		this.msgId = msgId;
		this.code = code;
		this.descript = descript;
		this.result = result;
	}

	/**
	 * @return the msgId
	 */
	public String getMsgId() {
		return msgId;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @return the descript
	 */
	public String getDescript() {
		return descript;
	}

	/**
	 * @return the result
	 */
	public Object getResult() {
		return result;
	}
	
	@Override
	public String toString() {
		return "msgId="+msgId+", code="+code+", descript="+descript+", result=[" + result.toString()+"]";
	}

}
